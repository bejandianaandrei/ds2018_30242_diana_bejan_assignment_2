package common;

import java.io.Serializable;

public class Car implements Serializable {
    private int year;
    private int engineSize;
    private double price;

    public Car() {
    }

    public Car(final int year, final int engineSize, final double price) {
        this.year = year;
        this.engineSize = engineSize;
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(final int year) {
        this.year = year;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(final int engineSize) {
        this.engineSize = engineSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }
}