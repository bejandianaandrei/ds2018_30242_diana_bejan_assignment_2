package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICarService extends Remote {

    float computePrice(final Car car) throws RemoteException;

    float computeTax(final Car car) throws RemoteException;

}