package common;

public class Constants {
    private Constants(){}
    public static final String CAR_SERVICE_NAME = "ICarService";
    public static final int PORT = 9000;
    public static final String SERVER_RUNNING_MESSAGE = "Server is running and available at port " + PORT;
    public static final int YEAR_SWITCH = 7;
    public static final int DEFAULT_PRICE = 0;
}