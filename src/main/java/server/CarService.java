package server;

import common.Car;
import common.Constants;
import common.ICarService;

import java.rmi.RemoteException;
import java.time.LocalDate;

public class CarService implements ICarService {

    public float computePrice(final Car car) throws RemoteException {
        if (LocalDate.now().getYear() - car.getYear() >= Constants.YEAR_SWITCH) {
            return Constants.DEFAULT_PRICE;
        }
        return (float) (car.getPrice() - (car.getPrice() / Constants.YEAR_SWITCH) * (LocalDate.now().getYear() - car.getYear()));
    }

    public float computeTax(final Car car) throws RemoteException {
        final int engineSize = car.getEngineSize();
        return ((float) engineSize / 200) * getSum(engineSize);
    }

    private int getSum(final int engineSize) {
        if (engineSize <= 1600)
            return 9;
        if (engineSize <= 2000)
            return 18;
        if (engineSize <= 2600)
            return 72;
        if (engineSize <= 3000)
            return 144;
        return 290;

    }
}