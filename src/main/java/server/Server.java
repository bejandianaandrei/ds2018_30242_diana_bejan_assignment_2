package server;

import common.Constants;
import common.ICarService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public static void main(final String ...args) {
        try {
            final CarService carService = new CarService();
            final ICarService carServiceStub = (ICarService) UnicastRemoteObject.exportObject(carService, Constants.PORT);
            final Registry registry = LocateRegistry.createRegistry(Constants.PORT);
            registry.bind(Constants.CAR_SERVICE_NAME, carServiceStub);
            System.out.println(Constants.SERVER_RUNNING_MESSAGE);
        } catch (final Exception e) {
            System.err.print("Server exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}