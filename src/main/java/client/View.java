package client;

import common.Car;
import common.ICarService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.time.LocalDate;

class View extends JFrame{

    private ICarService carService;
    private JTextField textYear, textEngineSize, textPrice;

    View(final ICarService carService) {
        final Dimension dimension = new Dimension(500, 400);
        this.setSize(dimension);
        this.carService = carService;
        final JLabel labelYear = new JLabel("Production year ");
        labelYear.setBounds((int)dimension.getWidth()/5, (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(labelYear);

        textYear = new JTextField();
        textYear.setBounds(3 * (int)dimension.getWidth()/5, (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(textYear);

        final JLabel labelEngineSize = new JLabel("Engine Size ");
        labelEngineSize.setBounds((int)dimension.getWidth()/5, 2 * (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(labelEngineSize);

        textEngineSize = new JTextField();
        textEngineSize.setBounds(3 * (int)dimension.getWidth()/5, 2 * (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(textEngineSize);

        final JLabel labelPrice = new JLabel("Car Price ");
        labelPrice.setBounds((int)dimension.getWidth()/5, 3 * (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(labelPrice);

        textPrice = new JTextField();
        textPrice.setBounds(3 * (int)dimension.getWidth()/5, 3 * (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        this.add(textPrice);

        final JButton buttonTax = new JButton("Compute Tax");
        buttonTax.setBounds((int)dimension.getWidth()/5, 5  * (int)dimension.getHeight()/10, (int)dimension.getWidth()/4, (int)dimension.getHeight()/10);
        buttonTax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    JOptionPane.showMessageDialog(null,
                            View.this.carService.computeTax(createCar()));
                } catch (final RemoteException e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(null, e1.getMessage());
                } catch (final NumberFormatException nex) {
                    JOptionPane.showMessageDialog(null, nex.getMessage());
                }
            }
        });
        this.add(buttonTax);

        final JButton buttonPrice = new JButton("Compute Selling Price");
        buttonPrice.setBounds(3 * (int)dimension.getWidth()/5, 5 * (int)dimension.getHeight()/10, (int)dimension.getWidth()/3, (int)dimension.getHeight()/10);
        buttonPrice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    JOptionPane.showMessageDialog(null,
                            View.this.carService.computePrice(createCar()));
                } catch (final RemoteException e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(null, e1.getMessage());
                } catch (final NumberFormatException nex) {
                    JOptionPane.showMessageDialog(null, nex.getMessage());
                }
            }
        });
        this.add(buttonPrice);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setVisible(true);
    }

    private Car createCar() throws IllegalArgumentException{
        final int year = Integer.parseInt(textYear.getText());
        final int engineSize = Integer.parseInt(textEngineSize.getText());
        final float price = Float.parseFloat(textPrice.getText());
        if(year > LocalDate.now().getYear()) {
            JOptionPane.showMessageDialog(null, "Car can't be produced in the future!!! Try again");
            throw new IllegalArgumentException();
        }

        if(year < 1900) {
            JOptionPane.showMessageDialog(null, "We didn't have cars then!!! Try again");
            throw new IllegalArgumentException();
        }

        if(engineSize < 0) {
            JOptionPane.showMessageDialog(null, "Engine size can't be negative!!! Try again");
            throw new IllegalArgumentException();
        }

        if(price < 0) {
            JOptionPane.showMessageDialog(null, "Price can not be negative, Sorry!!! Try again");
            throw new IllegalArgumentException();
        }

        return new Car(year, engineSize, price);
    }
}