package client;

import common.Constants;
import common.ICarService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
    public static void main(final String ...args) {
        try {
            final Registry registry = LocateRegistry.getRegistry(Constants.PORT);
            final ICarService carServiceStub = (ICarService) registry.lookup(Constants.CAR_SERVICE_NAME);
            final View mainWindow = new View(carServiceStub);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}